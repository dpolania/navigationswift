//
//  ViewController.swift
//  navigation
//
//  Created by david polania on 2/03/20.
//  Copyright © 2020 polania. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func nextAction(_ sender: Any) {
        self.pushView(NavigationMapOrigin.secondviewcontroller)
    }
    
}

