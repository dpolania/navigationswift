//
//  FourViewController.swift
//  navigation
//
//  Created by Macbook  on 24/04/20.
//  Copyright © 2020 polania. All rights reserved.
//

import UIKit

class FourViewController: UIViewController {

    @IBOutlet weak var textDataRequest: UILabel!
    var text : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textDataRequest.text = text ?? "No data"
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
