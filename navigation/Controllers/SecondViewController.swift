//
//  SecondViewController.swift
//  navigation
//
//  Created by david polania on 2/03/20.
//  Copyright © 2020 polania. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func nextAction(_ sender: Any) {
        self.pushView(NavigationMapOrigin.treeviewcontroller)
    }

}
