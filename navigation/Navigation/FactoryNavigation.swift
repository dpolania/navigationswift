//
//  FactoryNavigation.swift
//  navigation
//
//  Created by Macbook  on 24/04/20.
//  Copyright © 2020 polania. All rights reserved.
//

import Foundation
import UIKit

/**
 crea la instancia para utilizar el mapa de ruta creado
 */
struct FactoryNavigation : NavigationView{
    
    var mapNavigation: PresentationView?
    
    init(map : PresentationView){
        self.mapNavigation = map
    }
    func present() -> UIViewController{
        return self.mapNavigation?.getViewcontroller() ?? NavigationMapOrigin.viewcontroller.getViewcontroller()
    }
}
