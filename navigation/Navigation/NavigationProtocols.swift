//
//  NavigationProtocols.swift
//  navigation
//
//  Created by Macbook  on 24/04/20.
//  Copyright © 2020 polania. All rights reserved.
//

import Foundation
import UIKit

/**
   Este protocolo se encarga de enumara las vista las cuales
   deceo realizar la navegacion.
    con la funcion **getViewcontroller** se debe instanciar el controlador que se decea utilizar
    segun el enumerador  creado
 
  - Note: se debe tener en cuenta que se puede utilizar diferentes mapas de ruta dependiendo del orden
  que se le quiera dar a la aplicacion, se recomienda si la aplicacion es muy grande se utilice mas
  de una.
 
*/
protocol PresentationView{
    func getViewcontroller()-> UIViewController
}

protocol NavigationView{
    var mapNavigation : PresentationView? { get set }
    func present() -> UIViewController
}
