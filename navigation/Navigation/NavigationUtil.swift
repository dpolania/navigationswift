//
//  navigationUtil.swift
//  navigation
//
//  Created by david polania on 2/03/20.
//  Copyright © 2020 polania. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController{

    func pushView(_ ruteNavigation : PresentationView){
        let controller = FactoryNavigation(map: ruteNavigation).present()
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func popView(controller : UIViewController){
        for view in navigationController?.viewControllers ?? [] {
            if view.isKind(of: controller.classForCoder) {
                self.navigationController?.popToViewController(view, animated: true)
            }
        }
    }
}
