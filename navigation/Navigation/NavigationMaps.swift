//
//  NavigationMaps.swift
//  navigation
//
//  Created by Macbook  on 24/04/20.
//  Copyright © 2020 polania. All rights reserved.
//

import Foundation
import UIKit

/**
 En estos enumeradores que extiende del protocolo *PresentationView* se debe utilizar como el mapa de la navegacion
 con el objetivo de tener un archivo de ruta de nuestra aplicacion.
 */
 enum NavigationMapOrigin : String,PresentationView{
    
    case viewcontroller  = "ViewController"
    case secondviewcontroller = "SecondViewController"
    case treeviewcontroller = "TreeViewController"
    
    func getViewcontroller() -> UIViewController {
        var controller = UIViewController()
        switch self {
        case .viewcontroller:
            controller = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: self.rawValue)
        case .secondviewcontroller:
            controller = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: self.rawValue)
        case .treeviewcontroller:
            controller = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: self.rawValue)
        }
        return controller 
    }
}

/**
 Ejemplo de mapa para enviar con parametros el cual no tiene la misma estructura
 ya que el protocolo **String** no permite Associated Values en el enumerador.
 */
enum NavigationMapParams:PresentationView{
    case fourviewcontroller(String)
    
    func getViewcontroller() -> UIViewController {
        switch self {
        case .fourviewcontroller(let text):
            let controller : FourViewController  = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "FourViewController") as! FourViewController
            controller.text = text
            return controller
        }
    }
}
